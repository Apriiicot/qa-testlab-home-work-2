import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestAutorizationForm {
    public static void main(String[] args) {
        WebDriver driver = Util.initCromeDriver();
        login(driver);
        logout(driver);
    }

    public static void login(WebDriver driver){
        if(driver != null){
            driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

            WebElement field = driver.findElement(By.id("email"));
            field.sendKeys("webinar.test@gmail.com");

            WebElement password = driver.findElement(By.id("passwd"));
            password.sendKeys("Xcg7299bnSmMuRLp9ITw");

            WebElement submitLogin = driver.findElement(By.name("submitLogin"));
            submitLogin.click();
            Util.sleep(2000);
        }else {
            throw new RuntimeException("Can't login, webdriver is null");
        }
    }

    public static void logout(WebDriver driver){
        if(driver != null) {
            WebElement preference = driver.findElement(By.className("employee_avatar_small"));
            Util.sleep(2000);

            preference.click();
            Util.sleep(2000);

            WebElement logout = driver.findElement(By.id("header_logout"));
            logout.click();
        }else {
            throw new RuntimeException("Can't login, webdriver is null");
        }
    }
}
