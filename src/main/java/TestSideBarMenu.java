import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class TestSideBarMenu {
    public static void main(String[] args) {
        WebDriver driver = Util.initCromeDriver();
        login(driver);
        checkingSideBarElements(driver, findSideBarElements(driver));
    }

    public static void login(WebDriver driver){
        if(driver != null){
            driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

            WebElement field = driver.findElement(By.id("email"));
            field.sendKeys("webinar.test@gmail.com");

            WebElement password = driver.findElement(By.id("passwd"));
            password.sendKeys("Xcg7299bnSmMuRLp9ITw");

            WebElement submitLogin = driver.findElement(By.name("submitLogin"));
            submitLogin.click();
            Util.sleep(2000);
        }else {
            throw new RuntimeException("Can't login, webdriver is null");
        }
    }

    private static void checkingSideBarElements(WebDriver driver, List<String> activeSideBarElementIds) {
        for (String elementId : activeSideBarElementIds) {
            String currentUrl = driver.getCurrentUrl();
            WebElement element = driver.findElement(By.id(elementId));
            System.out.println("----------------------------------------------");
            System.out.println("WebElement: " + element.getText());
            element.click();

            System.out.println("Title after click: " + driver.getTitle());
            Util.sleep(2000);
            driver.navigate().refresh();
            Util.sleep(2000);

            System.out.println("Title after refresh: " + driver.getTitle());
            System.out.println("----------------------------------------------");
            if (!currentUrl.equals(driver.getCurrentUrl())) {
                driver.navigate().back();
                Util.sleep(2000);
            }
        }
    }

    private static List<String> findSideBarElements(WebDriver driver) {
        List<WebElement> sideBarElements = driver.findElements(By.className("maintab"));
        List<String> activeSideBarElementIds = new ArrayList<String>();
        for (WebElement element : sideBarElements) {
            if (element.isDisplayed()) {
                activeSideBarElementIds.add(element.getAttribute("id"));
            }
        }
        return activeSideBarElementIds;
    }
}
