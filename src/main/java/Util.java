import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Util {
    public static WebDriver initCromeDriver() {
        System.setProperty("webdriver.chrome.driver", Util.class.getResource( "chromedriver.exe").getPath());
        return new ChromeDriver();
    }

    public static void sleep(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
